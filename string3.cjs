function toPrintMonth(string){
    let monthsArray=["January","February","March","April","May","June","July","August","September","October","November","December"]

    let splitString=string.split("/")
    
    let month=parseInt(splitString[1])

    return monthsArray[month-1]
}

module.exports=toPrintMonth;



