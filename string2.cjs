function convertIpAdress(givenString){
    let outputArray=givenString.split(".").map((string)=> (isNaN(Number(string))) ? 1 : parseInt(string))
    
    let result=outputArray.indexOf(1)!==-1 ? [] : outputArray;
   
    return result;
}

module.exports=convertIpAdress;