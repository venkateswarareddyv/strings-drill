function toGetFullName(object){
    let newString="";
    for(let key in object){
        let firstLetter=object[key][0].toUpperCase()
        let remainingName=object[key].slice(1,object[key].length).toLowerCase()

        newString=newString+(firstLetter+remainingName)+" "
    }
    return newString
}

module.exports=toGetFullName;